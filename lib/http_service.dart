import 'dart:convert';
import 'package:http/http.dart';
import 'post_model.dart';

class HttpService {
   var  postsURL = Uri.parse('http://10.0.2.2:8080/accounts');

  Future<List<Post>> getPosts() async {
    Response res = await get(postsURL as Uri);
    print(res.body);
    print(res.statusCode);
    if (res.statusCode == 200) {
      print("response oke");
      List<dynamic> body = jsonDecode(res.body);
      print("body");
      print(body);
      List<Post> posts = body
          .map(
            (dynamic item) => Post.fromJson(item),
      )
          .toList();

      return posts;
    } else {
      print("unable to retrieve post");
      throw "Unable to retrieve posts.";
    }
  }
}